package uz.zafarobod;

import uz.zafarobod.annotations.Human;
import uz.zafarobod.annotations.Move;
import uz.zafarobod.reflection.Person;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws InvocationTargetException, IllegalAccessException {
//        System.out.println( "Hello World!" );
//        System.out.println("Hello friend!");
//        System.out.println("For dev");

        Person person = new Person();
        person.setPersonAge(14);
        person.setPersonName("Ali");

        Class cl = person.getClass();
        System.out.println(cl.getName());

        Method[] methods = cl.getDeclaredMethods();
        for (Method method : methods) {
            if(method.isAnnotationPresent(Move.class)){
                Move annotation = method.getAnnotation(Move.class);
                for (int i = 0; i < annotation.times(); i++){
                    method.invoke(person);
                }
            }
        }

        Field[] fields = cl.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getName() + " " +  field.getType());
        }

        try {
            Field field = cl.getDeclaredField("personAge");
            System.out.println(field.getName());
            System.out.println(person.getPersonAge());
            field.setAccessible(true);
            field.set(person, 16);
            System.out.println(person.getPersonAge());
            field.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        if(cl.isAnnotationPresent(Human.class)){
            System.out.println("This is human");
        } else {
            System.out.println("I don't know who is this");
        }

    }

}
