package uz.zafarobod.reflection;

import uz.zafarobod.annotations.Human;
import uz.zafarobod.annotations.Move;

@Human
public class Person {
    private String personName;
    private int personAge;

    public Person(){}

    public Person(String personName, int personAge) {
        this.personName = personName;
        this.personAge = personAge;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        this.personAge = personAge;
    }

    @Move(times = 3)
    public void running(){
        System.out.println("I'm running!");
    }
}
